import React, { Component } from "react";
import style from "./glass.module.css";

export default class Glass_Ex extends Component {
  state = {
    glassObject: {
      id: null,
      price: null,
      name: "",
      url: "",
      desc: "",
      display: "none",
    },
    glassList: [
      {
        id: 1,
        price: 30,
        name: "GUCCI G8850U",
        url: "./glassesImage/v1.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 2,
        price: 50,
        name: "GUCCI G8759H",
        url: "./glassesImage/v2.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 3,
        price: 30,
        name: "DIOR D6700HQ",
        url: "./glassesImage/v3.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 4,
        price: 70,
        name: "DIOR D6005U",
        url: "./glassesImage/v4.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 5,
        price: 40,
        name: "PRADA P8750",
        url: "./glassesImage/v5.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 6,
        price: 60,
        name: "PRADA P9700",
        url: "./glassesImage/v6.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 7,
        price: 80,
        name: "FENDI F8750",
        url: "./glassesImage/v7.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 8,
        price: 100,
        name: "FENDI F8500",
        url: "./glassesImage/v8.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
      {
        id: 9,
        price: 60,
        name: "FENDI F4300",
        url: "./glassesImage/v9.png",
        desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      },
    ],
  };
  handleChangeGlass = (id) => {
    this.state.glassList.forEach((item) => {
      if (item.id == id) {
        this.setState({
          glassObject: {
            id: item.id,
            price: item.price,
            name: item.name,
            url: item.url,
            desc: item.desc,
            display: "block",
          },
        });
      }
    });
  };

  renderGlassList = () => {
    let glassListComponent = this.state.glassList.map((glass) => {
      let src = `./glassesImage/g${glass.id}.jpg`;

      return (
        <div className="col-2 d-flex align-items-center justify-content-center">
          <img
            style={{ width: "100px", cursor: "pointer" }}
            onClick={() => {
              this.handleChangeGlass(`${glass.id}`);
            }}
            src={src}
            alt=""
          />
        </div>
      );
    });

    return glassListComponent;
  };
  render() {
    return (
      <div className={style.backgroundImg}>
        <h2 className="py-4 bg-dark text-white">TRY GLASSES APP ONLINE</h2>
        {/* model */}
        <div className="mt-5 d-flex align-items-center justify-content-center">
          <div className="haveGlass position-relative">
            <img className="w-50" src="./glassesImage/model.jpg" alt="" />
            {/* change glass from here */}
            <img
              style={{ top: "77px", left: "180px", opacity: "0.7" }}
              className="w-25 position-absolute"
              src={this.state.glassObject.url}
              alt=""
            />
            <div
              style={{
                left: "120px",
                opacity: "0.7",
                display: `${this.state.glassObject.display}`,
              }}
              className="position-absolute bottom-0 text-start w-50 py-1 ps-3 pe-3 bg-info"
            >
              <p
                style={{ fontSize: "18px", margin: "0 auto" }}
                className="text-uppercase text-danger fw-bolder"
              >
                {this.state.glassObject.name}
              </p>
              <p
                style={{ fontSize: "13px", margin: "0 auto" }}
                className="text-white fw-bold"
              >
                Price: ${this.state.glassObject.price}.00
              </p>
              <p
                style={{ fontSize: "13px", margin: "0 auto" }}
                className="text-white fw-bold"
              >
                {this.state.glassObject.desc}
              </p>
            </div>
          </div>
          <div className="default">
            <img className="w-50" src="./glassesImage/model.jpg" alt="" />
          </div>
        </div>
        {/* glass */}
        <div className="mt-5 d-flex justify-content-center">
          <div className="bg-white w-75 row">{this.renderGlassList()}</div>
        </div>
      </div>
    );
  }
}
